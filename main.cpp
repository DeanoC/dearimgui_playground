// ImGui - standalone example application for GLFW + OpenGL 3, using programmable pipeline
// If you are new to ImGui, see examples/README.txt and documentation at the top of imgui.cpp.
// (GLFW is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan graphics context creation, etc.)
// (GL3W is a helper library to access OpenGL functions since there is no standard header to access modern OpenGL functions easily. Alternatives are GLEW, Glad, etc.)

#include "glm/common.hpp"
#include "glm/glm.hpp"
#include "glm/ext.hpp"

#include <imgui.h>
#include "imgui_impl_glfw_gl3.h"
#include <stdio.h>
#include <GL/gl3w.h>    // This example is using gl3w to access OpenGL functions (because it is small). You may use glew/glad/glLoadGen/etc. whatever already works for you.
#include <GLFW/glfw3.h>
#include <algorithm>
#include "json.h"
#include "ImGuizmo/ImGuizmo.h"

#if __APPLE__
    #include "TargetConditionals.h"
    #include "Optional/optional.hpp"
#include "guizmo_math.h"

#include "propertyeditor.h"

namespace std
    {
        using namespace std::experimental;
    }
#else
    #include <optional>
#endif

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error %d: %s\n", error, description);
}

namespace glm {

    void to_json(nlohmann::json& j, const vec2& p) {
        j = nlohmann::json{ p.x, p.y };
    }
    void to_json(nlohmann::json& j, const vec3& p) {
        j = nlohmann::json{ p.x, p.y, p.z };
    }
    void to_json(nlohmann::json& j, const vec4& p) {
        j = nlohmann::json{ p.x, p.y, p.z, p.w };
    }

    void from_json(const nlohmann::json& j, vec2& p) {
        p.x = j[0]; p.y = j[1];
    }
    void from_json(const nlohmann::json& j, vec3& p) {
        p.x = j[0]; p.y = j[1]; p.z = j[2];
    }
    void from_json(const nlohmann::json& j, vec4& p) {
        p.x = j[0]; p.y = j[1]; p.z = j[2]; p.w = j[3];
    }
}

#include <math.h>
#include <vector>

struct Viewport {
    float width;
    float height;
    float x;
    float y;
};
struct MatrixScreenTransforms
{

    MatrixScreenTransforms(glm::mat4 const& model_, glm::mat4 const& view_, glm::mat4 const& proj_)
    {
        setModel(model_);
        setView(view_);
        setProj(proj_);

        update();
    }
    glm::mat4 glmModel;
    glm::mat4 glmView;
    glm::mat4 glmProj;
    glm::mat4 glmMVP;
    glm::mat4 glmViewInverse;

    void setModel(glm::mat4 const& model_)
    {
        using namespace ImGuizmoMath;
        glmModel = model_;
        model = *(matrix_t*)&glmModel[0][0];
    }
    void setView(glm::mat4 const& view_)
    {
        using namespace ImGuizmoMath;
        glmView = view_;
        view = *(matrix_t*)&glmView[0][0];
    }
    void setProj(glm::mat4 const& proj_)
    {
        using namespace ImGuizmoMath;
        glmProj = proj_;
        proj = *(matrix_t*)&glmProj[0][0];
    }

    void update()
    {
        using namespace ImGuizmoMath;
        glmMVP = glmModel * glmProj * glmView;
        mvp = *(matrix_t*)&glmMVP[0][0];

        glmViewInverse = glm::inverse(glmView);
        viewInverse = *(matrix_t*)&glmViewInverse[0][0];
    }


    ImGuizmoMath::matrix_t model;
    ImGuizmoMath::matrix_t view;
    ImGuizmoMath::matrix_t proj;
    ImGuizmoMath::matrix_t mvp;

    ImGuizmoMath::matrix_t viewInverse;
};


//
//
// ImGuizmo example helper functions
//
//

void Frustum(float left, float right, float bottom, float top, float znear, float zfar, float *m16)
{
    float temp, temp2, temp3, temp4;
    temp = 2.0f * znear;
    temp2 = right - left;
    temp3 = top - bottom;
    temp4 = zfar - znear;
    m16[0] = temp / temp2;
    m16[1] = 0.0;
    m16[2] = 0.0;
    m16[3] = 0.0;
    m16[4] = 0.0;
    m16[5] = temp / temp3;
    m16[6] = 0.0;
    m16[7] = 0.0;
    m16[8] = (right + left) / temp2;
    m16[9] = (top + bottom) / temp3;
    m16[10] = (-zfar - znear) / temp4;
    m16[11] = -1.0f;
    m16[12] = 0.0;
    m16[13] = 0.0;
    m16[14] = (-temp * zfar) / temp4;
    m16[15] = 0.0;
}

void Perspective(float fovyInDegrees, float aspectRatio, float znear, float zfar, glm::mat4 &proj)
{
    float ymax, xmax;
    ymax = znear * tanf(fovyInDegrees * 3.141592f / 180.0f);
    xmax = ymax * aspectRatio;
    float m16[16];
    Frustum(-xmax, xmax, -ymax, ymax, znear, zfar, m16);
    proj = { m16[ 0], m16[ 1],m16[ 2],m16[ 3],
             m16[ 4], m16[ 5],m16[ 6],m16[ 7],
             m16[ 8], m16[ 9],m16[10],m16[11],
             m16[12], m16[13],m16[14],m16[15] };
}

void EditTransform(MatrixScreenTransforms& transforms)
{
    static ImGuizmo::OPERATION mCurrentGizmoOperation(ImGuizmo::ROTATE);
    static ImGuizmo::MODE mCurrentGizmoMode(ImGuizmo::WORLD);
    static bool useSnap = false;
    static float snap[3] = { 1.f, 1.f, 1.f };

    if (ImGui::IsKeyPressed(90))
        mCurrentGizmoOperation = ImGuizmo::TRANSLATE;
    if (ImGui::IsKeyPressed(69))
        mCurrentGizmoOperation = ImGuizmo::ROTATE;
    if (ImGui::IsKeyPressed(82)) // r Key
        mCurrentGizmoOperation = ImGuizmo::SCALE;
    if (ImGui::RadioButton("Translate", mCurrentGizmoOperation == ImGuizmo::TRANSLATE))
        mCurrentGizmoOperation = ImGuizmo::TRANSLATE;
    ImGui::SameLine();
    if (ImGui::RadioButton("Rotate", mCurrentGizmoOperation == ImGuizmo::ROTATE))
        mCurrentGizmoOperation = ImGuizmo::ROTATE;
    ImGui::SameLine();
    if (ImGui::RadioButton("Scale", mCurrentGizmoOperation == ImGuizmo::SCALE))
        mCurrentGizmoOperation = ImGuizmo::SCALE;
    ImGuizmoMath::matrix_t model;
    float matrixTranslation[3], matrixRotation[3], matrixScale[3];
    model = transforms.model;
    ImGuizmo::DecomposeMatrixToComponents(model, matrixTranslation, matrixRotation, matrixScale);
    ImGui::InputFloat3("Tr", matrixTranslation, 3);
    ImGui::InputFloat3("Rt", matrixRotation, 3);
    ImGui::InputFloat3("Sc", matrixScale, 3);
    ImGuizmo::RecomposeMatrixFromComponents(matrixTranslation, matrixRotation, matrixScale, model);

    if (mCurrentGizmoOperation != ImGuizmo::SCALE)
    {
        if (ImGui::RadioButton("Local", mCurrentGizmoMode == ImGuizmo::LOCAL))
            mCurrentGizmoMode = ImGuizmo::LOCAL;
        ImGui::SameLine();
        if (ImGui::RadioButton("World", mCurrentGizmoMode == ImGuizmo::WORLD))
            mCurrentGizmoMode = ImGuizmo::WORLD;
    }
    if (ImGui::IsKeyPressed(83))
        useSnap = !useSnap;
    ImGui::Checkbox("", &useSnap);
    ImGui::SameLine();

    switch (mCurrentGizmoOperation)
    {
        case ImGuizmo::TRANSLATE:
            ImGui::InputFloat3("Snap", &snap[0]);
            break;
        case ImGuizmo::ROTATE:
            ImGui::InputFloat("Angle Snap", &snap[0]);
            break;
        case ImGuizmo::SCALE:
            ImGui::InputFloat("Scale Snap", &snap[0]);
            break;
    }
    ImGuiIO& io = ImGui::GetIO();
    ImGuizmo::SetRect(0, 0, io.DisplaySize.x, io.DisplaySize.y);
    ImGuizmo::Manipulate(transforms.view, transforms.proj, mCurrentGizmoOperation, mCurrentGizmoMode, model, NULL, useSnap ? &snap[0] : NULL);

    float* m16 = model;
    glm::mat4 m = { m16[ 0], m16[ 1],m16[ 2],m16[ 3],
             m16[ 4], m16[ 5],m16[ 6],m16[ 7],
             m16[ 8], m16[ 9],m16[10],m16[11],
             m16[12], m16[10],m16[14],m16[15] };

    transforms.setModel(m);
    transforms.update();
}

void propertyEditor(nlohmann::json& j);
void propertyEditJsonObject(nlohmann::json &j, const nlohmann::json &metaObj);
void customDraw();
void DrawCube(glm::vec3 const& pos, MatrixScreenTransforms const& transforms);
void DrawSphere(glm::vec3 const& pos, float const radius, MatrixScreenTransforms const& transforms);
void DrawCylinder(glm::vec3 const& pos, const float radius, const float height, MatrixScreenTransforms const& transforms );

int main(int, char**)
{
    // Setup window
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
        return 1;
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    GLFWwindow* window = glfwCreateWindow(1280, 720, "ImGui OpenGL3 example", NULL, NULL);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync
    gl3wInit();

    // Setup ImGui binding
    ImGui::CreateContext();
    ImGui_ImplGlfwGL3_Init(window, true);

    // Setup style
    //ImGui::StyleColorsClassic();
    ImGui::StyleColorsDark();

    // Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them. 
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple. 
    // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Read 'extra_fonts/README.txt' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    //ImGuiIO& io = ImGui::GetIO();
    //io.Fonts->AddFontDefault();
    //io.Fonts->AddFontFromFileTTF("../../extra_fonts/Roboto-Medium.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../extra_fonts/Cousine-Regular.ttf", 15.0f);
    //io.Fonts->AddFontFromFileTTF("../../extra_fonts/DroidSans.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../extra_fonts/ProggyTiny.ttf", 10.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(font != NULL);

    bool show_demo_window = true;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
    nlohmann::json j;
    {
        using namespace nlohmann;

        glm::vec2 v2bomb{ 10.0f, 20.0f };
        glm::vec3 v3bomb{ 30.0f, 20.0f, 10.0f };
        glm::vec4 v4bomb{ 100.0f, 200.0f, 300.0f, 400.f };

        json json1 = R"(
            {
                "array_str": ["My", "Cat", "Ate", "My", "Homework"],
                "bob_i": 42,
                "bob_f": 42.34354,
                "bob_t": "Text",
                "ohmy": {
                    "random": 42,
                    "deeper": {
                        "bomb_jack": [1.0,2.0,3.0]
                    }
                }
            }
        )"_json;


        nlohmann::json j_subber;
        j_subber["deep"] = "they went to deep!";
        j_subber["found2"] = v2bomb;
        j_subber["found3"] = v3bomb;
        j_subber["found4"] = v4bomb;
        j_subber["rgb"] = glm::vec3{1,0,0};
        j_subber["rgba"] = glm::vec4{0,1,0,0.5f};
        j_subber["int2"] = v2bomb;
        json1["ohmy"]["deeper"]["subber"] = j_subber;

        j = json1;
    }
    nlohmann::json boo_meta;
    boo_meta["label"] = "Boo!";
    boo_meta["min"] = 0;
    boo_meta["max"] = 100;

    nlohmann::json meta = R"(
    {
        "array_str": { "mut": true },
        "rgb": { "type": "colour" },
        "rgba": { "type": "colour" },
        "int2": {
            "type": "integer",
            "mutate": true,
            "tooltip": "2 of the finist ints you've seen today!"
        },
        "bob_i": {
            "tooltip": "bob_i is not mutable and sad!",
            "mut": false
        },
        "bob_t": {
            "label": "BBBobbb",
            "mut": true,
            "tooltip": "Bob is mutable and proud of it!"
        }
    })"_json;

    meta["random"] = boo_meta;

    j["__metadata__"] = meta;

    printf("Before:\n");
    printf("%s\n", j.dump().c_str());

    glm::mat4 objectMatrix =
            { 1.f, 0.f, 0.f, 0.f,
              0.f, 1.f, 0.f, 0.f,
              0.f, 0.f, 1.f, 0.f,
              0.f, 0.f, 0.f, 1.f };

    glm::mat4 cameraView =
            { 1.f, 0.f, 0.f, 0.f,
              0.f, 1.f, 0.f, 0.f,
              0.f, 0.f, 1.f, 0.f,
              1.f, -1.f, -4.f, 1.f };

    glm::mat4 cameraProjection;

    MatrixScreenTransforms transforms(objectMatrix, cameraView, cameraProjection);

    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();
        ImGui_ImplGlfwGL3_NewFrame();
/*
        ImGuiIO& io = ImGui::GetIO();
        Perspective(45.f, io.DisplaySize.x / io.DisplaySize.y, 0.1f, 100.f, cameraProjection);
        transforms.setProj(cameraProjection);

        ImGuizmo::BeginFrame();

        const ImU32 flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoBringToFrontOnFocus;
        ImGui::SetNextWindowSize(io.DisplaySize);
        ImGui::PushStyleColor(ImGuiCol_WindowBg, 0);
        ImGui::Begin("gizmo", NULL, flags);


//        DrawCube(transforms);
//        DrawSphere(glm::vec3{0,0,0}, 1.0f, transforms);
//        DrawCylinder( glm::vec3{0,0,0}, 1.0f, 3.0f, transforms);
        ImGui::End();
        ImGui::PopStyleColor();

        EditTransform(transforms);*/



        propertyeditor::propertyEditor(j);

//        // 3. Show the ImGui demo window. Most of the sample code is in ImGui::ShowDemoWindow().
//        if (show_demo_window)
//        {
//            ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiCond_FirstUseEver); // Normally user code doesn't need/want to call this because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
//            ImGui::ShowDemoWindow(&show_demo_window);
//        }

//        customDraw();

        // Rendering
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui::Render();

        glfwSwapBuffers(window);
    }
    printf("After:\n");
    printf("%s\n", j.dump().c_str());

    // Cleanup
    ImGui_ImplGlfwGL3_Shutdown();
    ImGui::DestroyContext();
    glfwTerminate();

    return 0;
}

void customDraw() {

    ImGui::Begin("Custom Draw");

    static ImVec4 col = ImVec4(1.0f,1.0f,0.4f,1.0f);
    ImGui::ColorEdit3("Color", &col.x);
    const ImVec2 p = ImGui::GetCursorScreenPos();
    const ImU32 col32 = ImColor(col);

    ImDrawList* drawList = ImGui::GetWindowDrawList();
    ImVec2 const windowSize = ImGui::GetWindowSize();
    ImVec2 const center( p.x + windowSize.x*0.5f, p.y + windowSize.y*0.5f);
    drawList->AddCircleFilled(center, std::min(windowSize.x,windowSize.y)/20, col32, 50);

    ImGui::End();

}
ImVec2 worldToPos(glm::vec3 const& worldPos, glm::mat4x4 const& mat, Viewport const& viewport)
{
    glm::vec4 trans;
    trans = mat * glm::vec4{ worldPos.x, worldPos.y, worldPos.z, 1.0f };
    trans *= 0.5f / trans.w;
    trans += glm::vec4 { 0.5f, 0.5f, 0.0f, 0.0f };
    trans.y = 1.f - trans.y;
    trans *= glm::vec4 { viewport.width, viewport.height, 0.0f, 0.0f };
    trans += glm::vec4 { viewport.x, viewport.y, 0.0f, 0.0f };
    return ImVec2(trans.x, trans.y);
}

void DrawCube(glm::vec3 const& pos, MatrixScreenTransforms const& transforms )
{
    using namespace ImGuizmoMath;
    static const vec_t directionUnary[3] = {
            makeVect(1.f, 0.f, 0.f),
            makeVect(0.f, 1.f, 0.f),
            makeVect(0.f, 0.f, 1.f)
    };
    ImGuiIO& io = ImGui::GetIO();


    ImGuizmoMath::Viewport viewport { io.DisplaySize.x, io.DisplaySize.y, 0, 0 };

    ImDrawList* drawList = ImGui::GetWindowDrawList();

    for (int iFace = 0; iFace < 6; iFace++)
    {
        const int normalIndex = (iFace % 3);
        const int perpXIndex = (normalIndex + 1) % 3;
        const int perpYIndex = (normalIndex + 2) % 3;
        const float invert = (iFace > 2) ? -1.f : 1.f;

        const vec_t faceCoords[4] = { directionUnary[normalIndex] + directionUnary[perpXIndex] + directionUnary[perpYIndex],
                                      directionUnary[normalIndex] + directionUnary[perpXIndex] - directionUnary[perpYIndex],
                                      directionUnary[normalIndex] - directionUnary[perpXIndex] - directionUnary[perpYIndex],
                                      directionUnary[normalIndex] - directionUnary[perpXIndex] + directionUnary[perpYIndex],
        };

        // clipping
        bool skipFace = false;
        for (unsigned int iCoord = 0; iCoord < 4; iCoord++)
        {
            vec_t camSpacePosition;
            camSpacePosition.TransformPoint(faceCoords[iCoord] * 0.5f * invert, transforms.mvp);
            if (camSpacePosition.z < 0.001f)
            {
                skipFace = true;
                break;
            }
        }
        if (skipFace)
            continue;

        // 3D->2D
        ImVec2 faceCoordsScreen[4];
        for (unsigned int iCoord = 0; iCoord < 4; iCoord++)
            faceCoordsScreen[iCoord] = ImGuizmoMath::worldToPos((faceCoords[iCoord] * 0.5f * invert), transforms.mvp, viewport);

        // back face culling
        vec_t cullPos, cullNormal;
        cullPos.TransformPoint(faceCoords[0] * 0.5f * invert, transforms.model);
        cullNormal.TransformVector(directionUnary[normalIndex] * invert, transforms.model);
        float dt = Dot(Normalized(cullPos - transforms.viewInverse.v.position), Normalized(cullNormal));
        if (dt>0.f)
            continue;

        // draw face with lighter color
        drawList->AddPolyline(faceCoordsScreen, 4, 0xFFFFFFFF,true,1.0f);
    }
}


void DrawSphere(glm::vec3 const& pos, float const radius, MatrixScreenTransforms const& transforms)
{
    static const unsigned int NUM_DIVISIONS = 50;

    ImDrawList* drawList = ImGui::GetWindowDrawList();
    ImGuiIO& io = ImGui::GetIO();

    Viewport viewport { io.DisplaySize.x, io.DisplaySize.y, 0, 0 };

    glm::vec3 radialX(0, radius, 0);
    glm::vec3 radialZ(0, radius, 0);

    glm::mat3x3 rotateX = glm::rotate( 2 * glm::pi<float>() / float(NUM_DIVISIONS),
                                       glm::vec3{0.0f,0.0f,1.0f} );
    glm::mat3x3 rotateZ = glm::rotate( 2 * glm::pi<float>() / float(NUM_DIVISIONS),
                                       glm::vec3{1.0f,0.0f,0.0f} );

    glm::vec3 x0(radialX);
    glm::vec3 z0(radialZ);

    for( unsigned int i=0;i < NUM_DIVISIONS;++i)
    {
        glm::vec3 x1 = rotateX * x0;
        glm::vec3 z1 = rotateZ * z0;
        glm::vec3 po[4] = { pos + x0, pos + x1, pos + z0, pos + z1 };

        ImVec2 coordsScreen[4];
        for (unsigned int iCoord = 0; iCoord < 4; iCoord++)
            coordsScreen[iCoord] = worldToPos(po[iCoord], transforms.glmMVP, viewport);

        drawList->AddPolyline(coordsScreen, 2, 0xFFFFFFFF, false, 1.0f);
        drawList->AddPolyline(coordsScreen+2, 2, 0xFFFFFFFF, false, 1.0f);

        x0 = x1;
        z0 = z1;
    }
}

void DrawCylinder(glm::vec3 const& pos,  const float radius, const float height, MatrixScreenTransforms const& transforms )
{
    static const unsigned int NUM_DIVISIONS = 20;
    ImDrawList* drawList = ImGui::GetWindowDrawList();
    ImGuiIO& io = ImGui::GetIO();
    Viewport viewport { io.DisplaySize.x, io.DisplaySize.y, 0, 0 };

    glm::vec3 radialZ(0, radius, 0);
    glm::vec3 offset(0, 0, height/2);

    glm::mat3x3 rotateX = glm::rotate( 2 * glm::pi<float>() / float(NUM_DIVISIONS),
                                       glm::vec3{0.0f,0.0f,1.0f} );
    glm::mat3x3 rotateZ = glm::rotate( 2 * glm::pi<float>() / float(NUM_DIVISIONS),
                                           glm::vec3{1.0f,0.0f,0.0f} );

    glm::vec3  z0(radialZ);
    for( unsigned int i=0;i < NUM_DIVISIONS;++i) {
        glm::vec3  z1 = rotateZ * z0;

        glm::vec3  z0a = rotateX * z0;
        glm::vec3  z1a = rotateX * z1;

        glm::vec3  pz0 = z0a - offset;
        glm::vec3  pz1 = z1a - offset;
        glm::vec3  pz0o = z0a + offset;
        glm::vec3  pz1o = z1a + offset;

        glm::vec3 po[6] = { pos + pz0, pos + pz1, pos + pz0o, pos + pz1o, pos + pz0, pos + pz1o };

        ImVec2 coordsScreen[6];
        for (unsigned int iCoord = 0; iCoord < 6; iCoord++)
            coordsScreen[iCoord] = worldToPos(po[iCoord], transforms.glmMVP, viewport);

        drawList->AddPolyline(coordsScreen, 2, 0xFFFFFFFF, false, 1.0f);
        drawList->AddPolyline(coordsScreen+2, 2, 0xFFFFFFFF, false, 1.0f);
        drawList->AddPolyline(coordsScreen+4, 2, 0xFFFFFFFF, false, 1.0f);

        z0 = z1;
    }
}